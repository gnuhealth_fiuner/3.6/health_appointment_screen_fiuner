#-*- coding: utf-8 -*-

import re
import requests 
import json

from datetime import date, datetime, timedelta
from dateutil.relativedelta import relativedelta
from trytond.model import ModelView, ModelSingleton, ModelSQL, fields
from trytond.pool import Pool, PoolMeta
from trytond.transaction import Transaction
from trytond.pyson import Or, Eval, Not, Bool, Equal
import calendar

__all__ = ['AppointmentData']


class AppointmentData(metaclass = PoolMeta):    
    'Appointment Data'
    __name__ = 'gnuhealth.appointment'

    calling = fields.Boolean("calling")

    in_atention = fields.Boolean("In atention")

    @staticmethod
    def default_calling():
        return False

    @staticmethod
    def default_in_atention():
        return False    

    @classmethod
    def __setup__(cls):
        super(AppointmentData, cls).__setup__()
        cls._order.insert(0, ('appointment_date', 'DESC'))

        cls._buttons.update({
            'checked_in': {'invisible': Not(Equal(Eval('state'), 'confirmed'))}
            })

        cls._buttons.update({
            'no_show': {'invisible': Not(Equal(Eval('state'), 'confirmed'))}
            })

        cls._buttons.update({
            'call': {'visible': Not( Equal(Eval('state'), 'confirmed'))}})

        cls._buttons.update({
            'in_atention': {'visible': Not( Equal(Eval('state'), 'confirmed'))}})
   
    @classmethod
    @ModelView.button
    def call(cls, AppointmentData):
        pool = Pool()
        Appointments = pool.get('gnuhealth.appointment')
        User = Pool().get('res.user')
        user = User(Transaction().user)

        start = datetime.today().replace(hour=0,minute=0,second=0)
        final = datetime.today().replace(hour=23,minute=59,second=59)
        appointments = Appointments.search([
         	('appointment_date','>=',start),
            ('appointment_date','<',final),
            ('healthprof.id','=',AppointmentData[0].healthprof.id)
            ])
        
        if AppointmentData[0].healthprof.name.internal_user.id == user.id:
            patient_calling =   {'patient':AppointmentData[0].patient.name.name+", "+AppointmentData[0].patient.name.lastname,
                            'professional':AppointmentData[0].healthprof.name.name+", "+AppointmentData[0].healthprof.name.lastname,
                            'specialty': AppointmentData[0].healthprof.main_specialty.specialty.name,
                            'time':AppointmentData[0].appointment_date.strftime('%H:%M'),
                            'state':'calling'
                            }

            index = 0
            for i in range(0,len(appointments)):
                if AppointmentData[0].patient == appointments[i].patient:
                    index = i - 1 

            if index >= 0:
                if AppointmentData[0].patient != appointments[index].patient:
                    patient_next =      {'patient':appointments[index].patient.name.name+", "+appointments[index].patient.name.lastname,
                                    'professional':appointments[index].healthprof.name.name+", "+appointments[index].healthprof.name.lastname,
                                    'specialty': appointments[index].healthprof.main_specialty.specialty.name,
                                    'time':appointments[index].appointment_date.strftime('%H:%M'),
                                    'state':'next'
                                    }
                else: 
                    patient_next =      {'patient':'',
                                    'professional':'',
                                    'specialty': '',
                                    'time':'',
                                    'state':'next'
                                    }  
            else: 
                patient_next =      {'patient':'',
                                'professional':'',
                                'specialty': '',
                                'time':'',
                                'state':'next'
                                }  

            appointments_data = {'data':
                                    [patient_calling,
                                    patient_next],
                                    'institution':AppointmentData[0].institution.name.name
                                    }
    # Configuramos el socket al cual vamos a enviar el webhook
    #TODO menu de configuración en el sistema
            
    
            webhook_url = "http://localhost:8010/appointments"
            r = requests.post(webhook_url, data=json.dumps(appointments_data), headers={'Content-Type':'application/json'})

        else:
            cls.raise_user_error('Cada profesional solo puede llamar a sus pacientes')
     
    @classmethod
    @ModelView.button
    def in_atention(cls, AppointmentData):
        pool = Pool()
        Appointments = pool.get('gnuhealth.appointment')
        User = Pool().get('res.user')
        user = User(Transaction().user)
        
        start = datetime.today().replace(hour=0,minute=0,second=0)
        final = datetime.today().replace(hour=23,minute=59,second=59)
        appointments = Appointments.search([
                                                ('appointment_date','>=',start),
                                                ('appointment_date','<',final),
                                                ('healthprof.name.internal_user.id','=',user.id),
                                                ('state','=','confirmed')
                                                ])
        if AppointmentData[0].healthprof.name.internal_user.id == user.id:
            cls.write(AppointmentData, {'state': 'checked_in'})
        
            patient_in_atention = {'patient':AppointmentData[0].patient.name.name+", "+AppointmentData[0].patient.name.lastname,
                            'professional':AppointmentData[0].healthprof.name.name+", "+AppointmentData[0].healthprof.name.lastname,
                            'specialty': AppointmentData[0].healthprof.main_specialty.specialty.name,
                            'time':AppointmentData[0].appointment_date.strftime('%H:%M'),
                            'state':'in_atention'
                            }

            index = 0
            for i in range(0,len(appointments)):
                if AppointmentData[0].patient == appointments[i].patient:
                    index = i - 1 

            if index >= 0 and index<len(appointments):
                if AppointmentData[0].patient != appointments[index].patient:
                    patient_next =      {'patient':appointments[index].patient.name.name+", "+appointments[index].patient.name.lastname,
                                    'professional':appointments[index].healthprof.name.name+", "+appointments[index].healthprof.name.lastname,
                                    'specialty': appointments[index].healthprof.main_specialty.specialty.name,
                                    'time':appointments[index].appointment_date.strftime('%H:%M'),
                                    'state':'next'
                                    }
                else: 
                    patient_next =      {'patient':'',
                                    'professional':'',
                                    'specialty': '',
                                    'time':'',
                                    'state':'next'
                                    }  
            else: 
                patient_next =      {'patient':'',
                                'professional':'',
                                'specialty': '',
                                'time':'',
                                'state':'next'
                                }  

            appointments_data = {'data':
                                    [patient_in_atention,
                                    patient_next],
                                    'institution':AppointmentData[0].institution.name.name
                                    }
    # Configuramos el socket al cual vamos a enviar el webhook
    #TODO menu de configuración en el sistema

            
            
            webhook_url = "http://localhost:8010/appointments"
            r = requests.post(webhook_url, data=json.dumps(appointments_data), headers={'Content-Type':'application/json'})

        else:
            cls.raise_user_error('Cada profesional solo puede llamar a sus pacientes')


